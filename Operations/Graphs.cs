﻿using MapDefinitions;
using System.Collections.Generic;
using System.Linq;

namespace Operations
{
    public static class Graphs
    {
        public static (bool Found, Point[] Points) Amplitude(Point root, Point search, Definitions defs)
        {
            //root: nó de origem ou raiz
            //search: nó de busca ou destino
            //defs: todas as definições de nó obtidas no sistema

            var completeList = new List<Point>() { root };  //Lista completa, iniciada com o nó raiz
            var checkQueue = new Queue<Point>();             //Fila de analise
            checkQueue.Enqueue(root);                        //adicione o nó raiz
            var visitedList = new List<Point>();            //Lista de visitados, iniciada vazia

            bool found = false;

            while (checkQueue.Any())
            {
                if (found = AmplitudeProc(visitedList, checkQueue, completeList, defs, search))
                    break;
            }

            return (found, completeList.ToArray()); //retorna a lista completa
        }

        private static bool AmplitudeProc(List<Point> visitedList, Queue<Point> checkList, List<Point> completeList, Definitions defs, Point search)
        {
            var parent = checkList.Peek();       //o primeiro nó desta fila sempre é analisado a cada interação (nó pai)
            var childs = defs.GetChilds(parent); //retornamos todos os filhos do nó pai e o colocamos na lista de filhos

            visitedList.Add(parent);                 //o nó pai é inserido a lista de visitados
            GraphsHelper.RemoveFromList(visitedList, childs);  //cada correspondencia da lista de visitados com os filhos é removida da lista de filhos
            GraphsHelper.RemoveFromList(completeList, childs); //cada correspondecia da lista completa com os filhos é removida da lista de filhos

            //nesse ponto temos certeza que todos filhos na lista não estão na lista completa nem na lista de remoção

            completeList.AddRange(childs); //todos os filhos são inseridos na lista completa
            foreach (var child in childs)
                checkList.Enqueue(child);  //todos os filhos são inseridos na fila de analise

            var founded = checkList.FirstOrDefault(c => c.Index == search.Index);

            if (founded != null) //se houver um nó na fila de analise que seja o nó de busca
            {
                //o nó de busca foi encontrado na fila de análise
                //a referência do nó de busca é atualizada com o nó pai atual
                //para simplificar a geração do caminho

                search.Parent = parent; //atualiza a busca com o registro do pai e lhe define o nível
                founded.IsSearch = true;
                return true;
            }

            checkList.Dequeue();      //o nó pai é removido da fila de análise

            return false;
        }

        public static (bool Found, Point[] Points) Profundidade(Point root, Point search, Definitions defs)
        {
            var completeList = new List<Point>() { root }; //lista completa, iniciada com root
            var currentStack = new Stack<Point>(); //pilha de atuais, analisa os nós do topo
            var visitedList = new List<Point>(); //lista de visitados

            bool found = false;

            currentStack.Push(root); //o nó atual da pilha é inserido

            while (currentStack.Any())
            {
                var parent = currentStack.Pop(); //o primeiro nó da pilha é removido da pilha
                var childs = defs.GetChilds(parent); //todos os nós filhos são obtidos

                visitedList.Add(parent); //o nó pai é inserido a lista de visitados
                GraphsHelper.RemoveFromList(visitedList, childs, parent, true); //cada correspondencia da lista de visitados com os filhos é removida da lista de filhos
                GraphsHelper.RemoveFromList(completeList, childs, parent, true); //cada correspondecia da lista completa com os filhos é removida da lista de filhos

                if (childs.Count == 0) //se não restarem filhos
                    continue; //partimos para o próximo item da pilha

                foreach (var c in childs)
                {
                    currentStack.Push(c); //insere o nó ao topo da pilha
                    completeList.Add(c);  //insere o nó a lista completa

                    found = c.Index == search.Index; //compara os valores para determinar se foi encontrado

                    if (found) //interrompa se encontrado
                    {
                        c.IsSearch = true;
                        break;
                    }
                }

                if (found)
                {
                    search.Parent = parent; //atualiza a busca com o registro do pai e lhe define o nível
                    break;
                }

            }

            return (found, completeList.ToArray());
        }

        public static (bool Found, Point[] Points) ProfundidadeLimitada(Point root, Point search, Definitions defs, int limit)
        {
            var completeList = new List<Point>() { root }; //lista completa, iniciada com root
            var currentStack = new Stack<Point>(); //pilha de atuais, analisa os nós do topo
            var visitedList = new List<Point>(); //lista de visitados

            bool found = false;

            currentStack.Push(root); //o nó atual da pilha é inserido

            while (currentStack.Any())
            {
                var parent = currentStack.Pop(); //o primeiro nó da pilha é removido da pilha
                var childs = defs.GetChilds(parent); //todos os nós filhos são obtidos

                visitedList.Add(parent); //o nó pai é inserido a lista de visitados
                GraphsHelper.RemoveFromList(visitedList, childs, parent, true); //cada correspondencia da lista de visitados com os filhos é removida da lista de filhos
                GraphsHelper.RemoveFromList(completeList, childs, parent, true); //cada correspondecia da lista completa com os filhos é removida da lista de filhos

                if (childs.Count == 0) //se não restarem filhos
                    continue; //partimos para o próximo item da pilha

                foreach (var c in childs)
                {

                    if (c.Level > limit)  //se o nivel do filho for superior ao limite
                        break;            //partimos para o proximo item da pilha

                    currentStack.Push(c); //insere o nó ao topo da pilha
                    completeList.Add(c);  //insere o nó a lista completa

                    found = c.Index == search.Index; //compara os valores para determinar se foi encontrado

                    if (found) //interrompa se encontrado
                    {
                        c.IsSearch = true;
                        break;
                    }
                }

                if (found)
                {
                    search.Parent = parent; //atualiza a busca com o registro do pai e lhe define o nível
                    break;
                }

            }

            return (found, completeList.ToArray());
        }

        public static (bool Found, Point[] Points) AprofundamentoInterativo(Point root, Point search, Definitions defs)
        {
            var completeList = new List<Point>(); //lista completa
            var currentStack = new Stack<Point>(); //pilha de atuais, analisa os nós do topo
            var visitedList = new List<Point>(); //lista de visitados

            bool found = false;

            for (int limit = 1; !found; limit++)
            {
                currentStack.Push(root); //o nó atual da pilha é inserido
                completeList.Add(root);

                while (currentStack.Any())
                {
                    var parent = currentStack.Pop(); //o primeiro nó da pilha é removido da pilha
                    var childs = defs.GetChilds(parent); //todos os nós filhos são obtidos

                    visitedList.Add(parent); //o nó pai é inserido a lista de visitados
                    GraphsHelper.RemoveFromList(visitedList, childs, parent, true); //cada correspondencia da lista de visitados com os filhos é removida da lista de filhos
                    GraphsHelper.RemoveFromList(completeList, childs, parent, true); //cada correspondecia da lista completa com os filhos é removida da lista de filhos

                    if (childs.Count == 0) //se não restarem filhos
                        continue; //partimos para o próximo item da pilha

                    foreach (var c in childs) //ordena e percorre os nós de maneira decrescente, para que o primeiro
                    {                                                        //nó da pilha seja o primeiro nó da lista

                        if (c.Level > limit)  //se o nivel do filho for superior ao limite
                            break;            //partimos para o proximo item da pilha

                        currentStack.Push(c); //insere o nó ao topo da pilha
                        completeList.Add(c);  //insere o nó a lista completa

                        found = c.Index == search.Index; //compara os valores para determinar se foi encontrado

                        if (found) //interrompa se encontrado
                        {
                            c.IsSearch = true;
                            break;
                        }
                    }

                    if (found)
                    {
                        search.Parent = parent; //atualiza a busca com o registro do pai e lhe define o nível
                        break;
                    }

                }

                if (found)
                    break;

                GraphsHelper.ClearParents(completeList.ToArray());

                completeList.Clear();
                currentStack.Clear();
                visitedList.Clear();
            }
            return (found, completeList.ToArray());
        }

        public static (bool Found, Point[] Points) Bidirecional(Point root, Point search, Definitions defs)
        {
            var completeRootList = new List<Point>() { root.New() };     //Lista completa, iniciada com o nó raiz
            var rootQueue = new Queue<Point>();                           //Fila de analise do nó raiz
            rootQueue.Enqueue(root.New());                                //adicionado o nó raiz
            var visitedRootList = new List<Point>();                     //Visitados do nó raiz

            var completeSearchList = new List<Point>() { search.New() }; //Lista completa, iniciada com nó de busca
            var searchQueue = new Queue<Point>();                         //Fila de analise
            searchQueue.Enqueue(search.New());                            //adicionado o nó de busca
            var visitedSearchList = new List<Point>();                   //Visitados do nó de busca 

            bool found = false;
            Point pointFound = null;

            while (rootQueue.Any() || searchQueue.Any())
            {
                AmplitudeProc(visitedRootList, rootQueue, completeRootList, defs, search);      //Executa amplitude a partir do nó raiz
                AmplitudeProc(visitedSearchList, searchQueue, completeSearchList, defs, root);  //Executa amplitude a partir do nó de busca

                pointFound = completeSearchList.Where(x =>
                 completeRootList.FirstOrDefault(y => x.Index == y.Index) != null)
                    .FirstOrDefault();            //Encontra uma correspondência entre as listas completas de raiz e de busca 

                if (found = pointFound != null)   //Se existir uma corresponência
                    break;                        //interrompa execução
            }

            if (found)                                //Organiza a lista completa de raiz
            {
                while (pointFound.Parent != null && pointFound.Index != search.Index)
                {
                    var pfParent = pointFound.Parent.New();
                    pfParent.Parent = completeRootList.FirstOrDefault(x => x.Index == pointFound.Index);

                    pointFound = pointFound.Parent;

                    completeRootList.Add(pfParent);
                }

                search.Parent = completeRootList.FirstOrDefault(x => x.Index == search.Index).Parent;
            }

            return (found, completeRootList.ToArray());
        }

        public static (bool Found, Point[] Points) CustoUniforme(Point root, Point search, Definitions defs)
        {
            var completeList = new List<Point>() { root };
            var currentList = new List<Point>() { root };

            bool found = false;

            while (currentList.Any())
            {
                var parent = currentList[0];
                var childs = defs.GetChilds(parent).OrderBy(x => x.Value).ToList();

                if (parent.Index == search.Index)
                {
                    parent.IsSearch = found = true;
                    search.Parent = parent.Parent;
                    
                    break;
                }

                GraphsHelper.RemoveParents(parent, childs);

                var betterChilds = GraphsHelper.RemoveMostExpensives(completeList, childs);

                var bestFound = currentList.FirstOrDefault(x => x.Value < parent.Value);

                if (bestFound != null && bestFound != parent)
                {
                    currentList.Remove(parent);
                    continue;
                }

                currentList.AddRange(betterChilds);
                completeList.AddRange(betterChilds);
                
                currentList.Remove(parent);
                currentList = currentList.OrderBy(x => x.Value).ToList();
            }

            return (found, completeList.ToArray());
        }

        public static (bool Found, Point[] Points) Greedy(Point root, Point search, Definitions defs)
        {
            root.Value = (int)GraphsHelper.Heuristic(root, search);
            var completeList = new List<Point>() { root };
            var currentList = new List<Point>() { root };

            bool found = false;

            while (currentList.Any())
            {
                var parent = currentList[0];
                var childs = defs.GetChilds(parent)
                    .OrderBy(x => x.Value = (int)GraphsHelper.Heuristic(x, search))
                    .ToList();

                if (parent.Index == search.Index)
                {
                    parent.IsSearch = found = true;
                    search.Parent = parent.Parent;

                    break;
                }

                GraphsHelper.RemoveParents(parent, childs);

                var betterChilds = GraphsHelper.RemoveMostExpensives(completeList, childs);

                var bestFound = currentList.FirstOrDefault(x => x.Value < parent.Value);

                if (bestFound != null && bestFound != parent)
                {
                    currentList.Remove(parent);
                    continue;
                }

                currentList.AddRange(betterChilds);
                completeList.AddRange(betterChilds);

                currentList.Remove(parent);
                currentList = currentList.OrderBy(x => x.Value).ToList();
            }

            return (found, completeList.ToArray());
        }

        public static (bool Found, Point[] Points) A_Estrela(Point root, Point search, Definitions defs)
        {
            root.Value = (int)GraphsHelper.Heuristic(root, search);
            var completeList = new List<Point>() { root };
            var currentList = new List<Point>() { root };

            bool found = false;

            while (currentList.Any())
            {
                var parent = currentList[0];
                var childs = defs.GetChilds(parent)
                    .OrderBy(x => x.Value + (int)GraphsHelper.Heuristic(x, search))
                    .ToList();

                if (parent.Index == search.Index)
                {
                    parent.IsSearch = found = true;
                    search.Parent = parent.Parent;

                    break;
                }

                GraphsHelper.RemoveParents(parent, childs);

                var betterChilds = GraphsHelper.RemoveMostExpensives(completeList, childs);

                var bestFound = currentList.FirstOrDefault(x => x.Value < parent.Value);

                if (bestFound != null && bestFound != parent)
                {
                    currentList.Remove(parent);
                    continue;
                }

                currentList.AddRange(betterChilds);
                completeList.AddRange(betterChilds);

                currentList.Remove(parent);
                currentList = currentList.OrderBy(x => x.Value).ToList();
            }

            return (found, completeList.ToArray());
        }
    }
}
