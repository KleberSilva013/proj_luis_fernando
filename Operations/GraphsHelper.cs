﻿using MapDefinitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Operations
{
    internal static class GraphsHelper
    {
        internal static void RemoveFromList(IList<Point> source, IList<Point> childs, Point parent = null, bool useLevel = false)
        {
            foreach (var s in source)
            {
                var rem = childs.FirstOrDefault(x => x.Index == s.Index);

                if (rem == null)
                    continue;

                if (useLevel)
                {
                    if (s.Level > parent.Level)
                        continue;
                }

                childs.Remove(rem);
            }
        }

        internal static void ClearParents(Point[] points)
        {
            if (points != null)
            {
                foreach (var p in points)
                    p.Parent = null;
            }
        }

        internal static IList<Point> RemoveMostExpensives(IList<Point> source, IList<Point> childs)
        {
            var points = new List<Point>();

            foreach (var child in childs)
            {
                var cheapest = source.Where(x => x.Index == child.Index)
                    .FirstOrDefault(x => x.Value < child.Value);

                if (cheapest != null)
                    continue;

                points.Add(child);
            }

            return points;
        }

        internal static void RemoveParents(Point source, IList<Point> childs)
        {
            var parent = source;

            while(parent != null)
            {
                childs.Remove(childs.FirstOrDefault(x => x.Index == parent.Index));

                parent = parent.Parent;
            }
        }

        internal static double Heuristic(Point current, Point search)
        {
            return current.Coordinates.Heuristic = Math.Abs( (search.Coordinates.X - current.Coordinates.X)) +
                Math.Abs((search.Coordinates.Y - current.Coordinates.Y));
        }
    }
}
