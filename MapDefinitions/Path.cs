﻿using System.Collections;
using System.Collections.Generic;
using System.Windows.Controls.Primitives;

namespace MapDefinitions
{
    public class Path
    {
        private ButtonBase[] mPaths;
        public string Name { get; private set; }

        public Path(string name, params ButtonBase[] paths)
        {
            Name = name;
            mPaths = paths;
        }

        public void Enabled(bool enabled)
        {
            foreach(ButtonBase path in mPaths)
            {
                path.IsEnabled = enabled;
            }
        }
    }
}
