﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MapDefinitions
{
    public class Definitions
    {
        private IList<Point> mPointsObjects = new List<Point>();

        private IList<Path> mPathsObjects = new List<Path>();

        private static char[] Points { get; } = {
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
            'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T'
        };
        private static IList<char[]> Paths { get; } = new List<char[]>()
        {
            new char[]{ 'B', 'D', 'E', 'F', 'I', 'J', 'L', 'M', 'Q', 'R', 'T'},
            new char[]{ 'A', 'D', 'E', 'F', 'I', 'J', 'L', 'M', 'Q', 'R', 'T'},
            new char[]{ 'D', 'G', 'H', 'J', 'K', 'M', 'N', 'O', 'P', 'R', 'S'},
            new char[]{ 'A', 'B', 'C', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T'},
            new char[]{ 'A', 'B', 'D', 'F', 'I', 'J', 'L', 'M', 'Q', 'R', 'T'},
            new char[]{ 'A', 'B', 'D', 'E', 'I', 'J', 'L', 'M', 'Q', 'R', 'T'},
            new char[]{ 'C', 'D', 'H', 'J', 'K', 'M', 'N', 'O', 'P', 'R', 'S'},
            new char[]{ 'C', 'D', 'G', 'J', 'K', 'M', 'N', 'O', 'P', 'R', 'S'},
            new char[]{ 'A', 'B', 'D', 'E', 'F', 'J', 'L', 'M', 'Q', 'R', 'T'},
            new char[]{ 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T'},
            new char[]{ 'C', 'D', 'G', 'H', 'J', 'M', 'N', 'O', 'P', 'R', 'S'},
            new char[]{ 'A', 'B', 'D', 'E', 'F', 'I', 'J', 'M', 'Q', 'R', 'T'},
            new char[]{ 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'N', 'O', 'P', 'Q', 'R', 'S', 'T'},
            new char[]{ 'C', 'D', 'G', 'H', 'J', 'K', 'M', 'O', 'P', 'R', 'S'},
            new char[]{ 'C', 'D', 'G', 'H', 'J', 'K', 'M', 'N', 'P', 'R', 'S'},
            new char[]{ 'C', 'D', 'G', 'H', 'J', 'K', 'M', 'N', 'O', 'R', 'S'},
            new char[]{ 'A', 'B', 'D', 'E', 'F', 'I', 'J', 'L', 'M', 'R', 'T'},
            new char[]{ 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'S', 'T'},
            new char[]{ 'C', 'D', 'G', 'H', 'J', 'K', 'M', 'N', 'O', 'P', 'R'},
            new char[]{ 'A', 'B', 'D', 'E', 'F', 'I', 'J', 'L', 'M', 'Q', 'R'}
        };
        private static IList<int[]> PathsDistance { get; } = new List<int[]>()
        {
            new int[]{ 270, 400, 160, 350, 130, 410, 320, 380, 210, 110, 130},
            new int[]{ 270, 260, 140, 210, 170, 260, 180, 230, 110, 270, 220},
            new int[]{ 100, 380, 080, 070, 380, 340, 170, 310, 100, 390, 460},
            new int[]{ 400, 260, 100, 270, 040, 580, 210, 150, 060, 590, 130, 070, 300, 520, 150, 240, 160, 670, 340},
            new int[]{ 160, 140, 270, 220, 080, 280, 190, 250, 080, 160, 100},
            new int[]{ 350, 210, 040, 220, 100, 090, 080, 060, 190, 150, 290},
            new int[]{ 380, 580, 220, 310, 020, 120, 090, 110, 460, 090, 080},
            new int[]{ 080, 210, 220, 120, 230, 190, 060, 160, 090, 240, 310},
            new int[]{ 130, 170, 150, 080, 100, 160, 110, 090, 110, 070, 150},
            new int[]{ 410, 260, 070, 060, 280, 090, 310, 120, 160, 320, 140, 080, 350, 250, 160, 250, 170, 400, 350},
            new int[]{ 380, 590, 020, 230, 320, 130, 120, 120, 470, 080, 090},
            new int[]{ 320, 180, 130, 190, 080, 110, 140, 110, 160, 180, 260},
            new int[]{ 380, 230, 340, 070, 250, 060, 120, 190, 090, 080, 130, 110, 160, 080, 430, 220, 100, 210, 320},
            new int[]{ 170, 300, 090, 060, 350, 120, 160, 090, 180, 190, 040},
            new int[]{ 310, 320, 110, 160, 250, 120, 080, 090, 400, 130, 160},
            new int[]{ 100, 150, 460, 090, 160, 470, 430, 180, 400, 480, 550},
            new int[]{ 210, 110, 240, 080, 190, 110, 250, 160, 220, 210, 090},
            new int[]{ 110, 270, 390, 160, 160, 150, 090, 240, 070, 170, 080, 180, 100, 190, 130, 480, 210, 160, 130},
            new int[]{ 460, 670, 080, 310, 400, 090, 210, 040, 160, 550, 160},
            new int[]{ 130, 220, 340, 100, 290, 150, 350, 260, 320, 090, 130},
        };

        public IList<Point> GetChilds(Point point)
        {
            var list = new List<Point>();

            IList<char> points = Points;

            foreach (var p in Paths[point.Index])
            {
                var child = GetPointByIndex(points.IndexOf(p)).New();

                child.Parent = point;
                list.Add(child);
            }

            return list;
        }

        public void AddPoint(Point point)
        {
            if (mPointsObjects.FirstOrDefault(x => x.Index == point.Index) != null)
                throw new ArgumentException(nameof(point));

            mPointsObjects.Add(point);
        }

        public void RemovePoint(Point point)
        {
            var b = mPointsObjects.FirstOrDefault(x => x.Index == point.Index) ??
                throw new ArgumentException(nameof(point));

            mPointsObjects.Remove(b);
        }

        public char[] GetAllPoints()
        {
            return Points;
        }

        public void ClearAllParents()
        {
            foreach(var p in mPointsObjects)
            {
                p.Parent = null;
                p.Value = 0;
            }
        }

        public Point GetPointByIndex(int index)
        {
            return mPointsObjects.FirstOrDefault(x => x.Index == index) ??
                throw new ArgumentException(nameof(index));
        }

        public Point GetPointByName(char name)
        {
            return mPointsObjects.FirstOrDefault(x => x.Name == name) ??
                throw new ArgumentException(nameof(name));
        }

        public void AddPath(Path path)
        {
            if (mPathsObjects.FirstOrDefault(x => x.Name == path.Name) != null)
                throw new ArgumentException(nameof(path));

            mPathsObjects.Add(path);
        }

        public void RemovePath(Path path)
        {
            var b = mPathsObjects.FirstOrDefault(x => x.Name == path.Name) ??
                throw new ArgumentException(nameof(path));

            mPathsObjects.Remove(b);
        }

        public Path GetPathBy(char a, char b)
        {
            return mPathsObjects.Where(x => x.Name.Contains(a))
                .Where(x => x.Name.Contains(b))
                .FirstOrDefault() ??
                throw new ArgumentException(nameof(a) + nameof(b));
        }

        public char[] GetPathBy(int id)
        {
            return Paths[id];
        }

        public static int GetDistance(int parentIndex, char childName)
        {
            int childIndex = 0;
            
            foreach(var p in Paths[parentIndex])
            {
                if (p == childName)
                    break;

                childIndex++;
            }

            return PathsDistance[parentIndex][childIndex];
        }
    }
}
