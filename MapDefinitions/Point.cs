﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.Primitives;

namespace MapDefinitions
{
    public class Coordinates
    {
        public Coordinates(double x, double y)
        {
            X = Math.Round(x/10);
            Y = Math.Round(y/10);
        }

        public double X { get; set; }
        public double Y { get; set; }

        public double Heuristic { get; set; }

        public override string ToString()
        {
            return "[" + X + ", " + Y + "]";
        }
    }

    public class Point
    {
        private char mName;
        private int mLevel = 0;
        private int mValue = 0;
        private bool mIsRoot = false;
        private bool mIsSearch = false;

        private Point mParent = null;
        private ToggleButton mControl;

        public char Name => mName;
        public ToggleButton Control => mControl;
        public int Level => mLevel;
        public int Index { get; private set; }

        public bool IsRoot
        {
            get => mIsRoot;
            set
            {
                if (value)
                    mIsSearch = false;

                mIsRoot = value;
            }
        }
        public bool IsSearch
        {
            get => mIsSearch;
            set
            {
                if (value)
                    mIsRoot = false;

                mIsSearch = value;
            }
        }

        public Point Parent
        {
            get => mParent;
            set
            {
                mParent = value;

                if (mParent == null)
                {
                    mLevel = 0;
                    return;
                }

                mLevel = mParent.Level + 1;

                mValue = mParent.Value + Definitions.GetDistance(mParent.Index, Name);
            }
        }

        public int Value { get => mValue; set => mValue = value; }

        public Coordinates Coordinates { get; }

        public Point(int index, char name, ToggleButton control)
        {
            Index = index;
            mName = name;
            mControl = control;
            Coordinates = new Coordinates(mControl.Margin.Left, mControl.Margin.Top);

        }

        public Point New()
        {
            return new Point(Index, mName, mControl);
        }

        public override string ToString()
        {
            return Name.ToString() + "(" + this.Value + ")";
        }
    }
}
