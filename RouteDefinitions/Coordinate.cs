﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.Primitives;

namespace RouteDefinitions
{
    public class Coordinate : IMapObject
    {
        private double mSize;
        private SizeDirection mDirection;
        private ButtonBase mControl;
        private IList<Coordinate> mLinks = new List<Coordinate>();

        public double Size {
            get
            {
                switch (mDirection)
                {
                    case SizeDirection.HORIZONTAL:
                        return mControl.Width;
                    default:
                        return mControl.Height;
                }
            }
        }

        public SizeDirection Direction => mDirection;

        public Coordinate(ButtonBase control, SizeDirection direction)
        {
            mControl = control;
            mDirection = direction;
        }

        public void AddLinksRange(params Coordinate[] range)
        {
            foreach (var c in range)
                mLinks.Add(c);
        }

        public Coordinate[] GetLinks()
        {
            return mLinks.ToArray();
        }
    }
}
