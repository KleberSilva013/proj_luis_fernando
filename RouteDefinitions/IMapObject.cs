﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.Primitives;

namespace RouteDefinitions
{
    public enum SizeDirection
    {
        VERTICAL,
        HORIZONTAL,
        SQUARE
    }
    public interface IMapObject
    {
        double Size { get; }
        SizeDirection Direction { get; }
    }
}
