﻿using MapDefinitions;
using Operations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using Point = MapDefinitions.Point;

namespace projLuisFernando
{
    /// <summary>
    /// Interação lógica para MainWindow.xam
    /// </summary>
    public partial class MainWindow : Window
    {
        private Setup mSetup;
        private IList<Point> mCurrentPoints = new List<Point>();
        private IList<Path> mCurrentPaths = new List<Path>();
        private Point[] mSetted;
        private event PropertyChangedEventHandler PointsChanged;
        private int mPointsCount;
        private ButtonBase[] Paths => new ButtonBase[] {
                _1,_2,_3,_4,_5,_6,_7,_8,_9,_10,
                _11,_12,_13,_14,_15,_16,_17,_18,_19,_20,
                _21,_22,_23,_24,_25,_26,_27,_28,_29,_30,
                _31,_32,_33,_34,_35,_36,_37,_38,_39,_40,
                _41,_42,_43,_44,_45,_46,_47,_48,_49,_50,
                _51,_52,_53,_54,_55,_56,_57,_58,_59,_60,
                _61,_62,_63,_64,_65,_66,_67,_68,_69,_70,
                _71,_72,_73,_74,_75,_76,_77,_78,_79,_80,
                _81,_82,_83,_84,_85,_86,_87,_88,_89,_90,
                _91,_92,_93,_94,_95,_96,_97,_98,_99,_100,
                _101,_102,_103,_104,_105,_106,_107,_108,_109,_110,
                _111,_112,_113,_114,_115,_116,_117,_118,_119,_120,
                _121,_122,_123,_124,_125,_126,_127,_128,_129,_130,
                _131,_132,_133,_134,_135,_136,_137,_138,_139,_140,
                _141,_142,_143,_144,_145,_146,_147,_148,_149,_150,
                _151,_152,_153,_154,_155,_156,_157,_158,_159,_160,
                _161,_162,_163,_164,_165,_166,_167,_168,_169,_170,
                _171,_172
            };
        private ToggleButton[] Points => new ToggleButton[]
            {
                A, B, C, D, E, F, G, H, I, J,
                K, L, M, N, O, P, Q, R, S, T
            };

        public int PointsCount
        {
            get { return mPointsCount; }
            set
            {
                if (value != mPointsCount)
                {
                    mPointsCount = value;
                    Invoke_PointsChanged(nameof(PointsCount));
                }
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            mSetup = new Setup(this);
            PointsChanged += MainWindow_PointsChanged;
        }

        public Point Root { get => mCurrentPoints[0]; }
        public Point Search { get => mCurrentPoints[1]; }

        private void MainWindow_PointsChanged(object sender, PropertyChangedEventArgs e)
        {
            tbLog.Text = string.Empty;
            mSetup.Definitions.ClearAllParents();
            tvResult.Items.Clear();
            mSetted = null;

            ClearPaths();

            if (PointsCount > 2)
            {
                Root.Control.IsChecked = false;
                mCurrentPoints.RemoveAt(0);
                mPointsCount = mCurrentPoints.Count;
            }

            if (PointsCount == 2)
            {

                Root.IsRoot = true;
                Search.IsSearch = true;

                var result = SelectedMethod;

                if (!result.Found)
                {
                    tbLog.Text = "Resposta não encontrada";
                    return;
                }

                mSetted = result.Points;
                tbLog.Text = GetOutputString(mSetted) + "\n";

                tbLog.Text += "Caminho: \n";

                var act = Search;
                while (act != null)
                {
                    var a = act.Name;
                    var b = act.Parent;
                    tbLog.Text += "> " + a;
                    act = b;

                    if (b != null)
                        ShowPaths(a, b.Name);
                }

                SetTree(cbSearch.SelectedIndex > 4);
                ClearAllValues();
            }
        }

        private void ClearAllValues()
        {
            foreach (var point in mSetted)
                point.Value = 0;
        }

        private void ShowPaths(char a, char b)
        {
            var path = mSetup.Definitions.GetPathBy(a, b);
            path.Enabled(true);
            mCurrentPaths.Add(path);
        }

        private void ClearPaths()
        {
            foreach (Path path in mCurrentPaths)
            {
                path.Enabled(false);
            }

            mCurrentPaths.Clear();
        }
        private string GetOutputString(Point[] points)
        {
            var level = points.OrderByDescending(x => x.Level).First().Level;
            
            string text = "";
            
            for (int i = 0; i <= level; i++)
            {
                text += "Level " + i + ":\n";

                foreach (Point p in points.Where(x => x.Level == i))
                {
                    text += p.Name + ", ";
                }

                text += "\n";
            }

            return text;
        }

        public ToggleButton GetPoint(string name)
        {
            return Points.FirstOrDefault(x => x.Name == name);
        }

        public ButtonBase[] GetPath(params string[] names)
        {
            var path = new List<ButtonBase>();
            foreach (var name in names)
            {
                var p = Paths.FirstOrDefault(x => x.Name == "_" + name) ??
                    throw new ArgumentException(nameof(name));

                path.Add(p);
            }

            return path.ToArray();
        }

        private void Invoke_PointsChanged(string name = null)
        {
            PointsChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        private void Point_Clicked(object sender, RoutedEventArgs e)
        {
            var obj = (ToggleButton)sender;
            var point = mSetup.Definitions.GetPointByName(obj.Name[0]);

            if (obj.IsChecked == false)
            {
                mCurrentPoints.Remove(point);
                point.IsRoot = false;
                point.IsSearch = false;
            }
            else
                mCurrentPoints.Add(point);

            PointsCount = mCurrentPoints.Count;
        }

        private void cbSearch_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            var text = (cbSearch.SelectedItem as ListBoxItem).Content + "";

            if (text == null || lbSearchName == null)
                return;

            lbLimit.Visibility = tbLimit.Visibility = cbSearch.SelectedIndex == 2 ?
                Visibility.Visible : Visibility.Hidden;

            tbLimit.Text = 1.ToString();

            lbSearchName.Text = text.ToUpper();
        }

        private (bool Found, Point[] Points) SelectedMethod
        {

            get
            {
                switch (cbSearch.SelectedIndex)
                {
                    case 0:
                        return Graphs.Amplitude(Root, Search, mSetup.Definitions);
                    case 1:
                        return Graphs.Profundidade(Root, Search, mSetup.Definitions);
                    case 2:
                        int limit = int.Parse(tbLimit.Text);
                        return Graphs.ProfundidadeLimitada(Root, Search, mSetup.Definitions, limit);
                    case 3:
                        return Graphs.AprofundamentoInterativo(Root, Search, mSetup.Definitions);
                    case 4:
                        return Graphs.Bidirecional(Root, Search, mSetup.Definitions);
                    case 5:
                        return Graphs.CustoUniforme(Root, Search, mSetup.Definitions);
                    case 6:
                        return Graphs.Greedy(Root, Search, mSetup.Definitions);
                    case 7:
                        return Graphs.A_Estrela(Root, Search, mSetup.Definitions);
                    default:
                        return (false, new Point[] { });
                }
            }
        }

        private void SetTree(bool showValue = false)
        {
            foreach (var p in mSetted)
            {
                var header = " " + p.Name;

                if (showValue)
                    header += "(" + p.Value + ")";

                header += " ";

                TreeViewItem item = new TreeViewItem()
                {
                    Header = header,
                    BorderBrush = Brushes.Black,
                    BorderThickness = new Thickness(1),
                    FontFamily = new FontFamily("Courier New"),
                    FontSize = 18
                };

                item.IsExpanded = true;

                if (p.IsSearch)
                    item.Background = Brushes.Orange;

                var node = GetSubItem(p);

                if (node == null)
                {
                    if (p.IsRoot)
                        item.Background = Brushes.LightGreen;

                    tvResult.Items.Add(item);
                }
                else
                {
                    node.Items.Add(item);
                }
            }
        }

        private TreeViewItem GetSubItem(Point point)
        {
            TreeViewItem item = null;
            if (point.Parent != null)
            {
                item = GetSubItem(point.Parent);

                foreach (TreeViewItem node in item.Items)
                {
                    if (node.Header.ToString().Trim().Contains(point.Name))
                        return node;
                }
            }
            else
            {
                foreach (TreeViewItem node in tvResult.Items)
                {
                    if (node.Header.ToString().Trim().Contains(point.Name))
                        return node;
                }
            }

            return item;
        }
    }
}
